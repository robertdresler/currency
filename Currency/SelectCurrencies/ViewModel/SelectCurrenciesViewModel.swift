//
//  SelectCurrenciesViewModel.swift
//  Currency
//
//  Created by Robert Dresler on 03/05/2019.
//  Copyright © 2019 Robert Dresler. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

final class SelectCurrenciesViewModel: CurrencyViewModel {
    
    var manager = CurrencyManagerFactory.manager
    var currencySelected: (Currency) -> Void = { _ in }
    
    var bag = DisposeBag()
    
    var doneButtonPressed: () -> Void = {}
    
    let searchText = BehaviorRelay<String>(value: "")
    
    let currencySections = BehaviorRelay<[CurrencyAlphabetSection]>(value: [])
    private let currencies = BehaviorRelay<[Currency]>(value: [])
    
    required init() {
        setupBinding()
        currencySelected = { [weak self] currency in
            self?.manager.toogleIsSelected(of: currency)
        }
    }
    
    private func setupBinding() {
        
        manager.currencies.bind(to: currencies).disposed(by: bag)
        
        currencies.bind { [weak self] _ in
            self?.searchCurrencies()
            }.disposed(by: bag)
        
        searchText.bind { [weak self] _ in
            self?.searchCurrencies()
            }.disposed(by: bag)
    }
    
    private func searchCurrencies() {
        
        let filtered = !searchText.value.isEmpty ? currencies.value.filter({ $0.name.contains(searchText.value) }) : currencies.value
        
        let grouped = filtered.sorted(by: { $0.name < $1.name }).reduce([[Currency]]()) {
            guard var last = $0.last else { return [[$1]] }
            var collection = $0
            if last.first!.name.first == $1.name.first {
                last += [$1]
                collection[collection.count - 1] = last
            } else {
                collection += [[$1]]
            }
            return collection
        }
        
        let sectioned = grouped.compactMap { (currencies: [Currency]) -> CurrencyAlphabetSection? in
            guard let firstLetter = currencies[0].name.first else { return nil }
            
            let currenciesProjections = currencies.map {
                CurrencyProjection(code: $0.code, name: $0.name.uppercased(), highlightedField: $0.code, flagImageUrl: $0.flagImageURL, isHighlighted: $0.isSelected)
            }
            return CurrencyAlphabetSection(header: String(firstLetter), items: currenciesProjections)
        }
        
        currencySections.accept(sectioned)
    }
    
}
