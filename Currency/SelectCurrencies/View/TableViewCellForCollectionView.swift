//
//  TableViewCellForCollectionView.swift
//  Currency
//
//  Created by Robert Dresler on 03/05/2019.
//  Copyright © 2019 Robert Dresler. All rights reserved.
//

import UIKit
import SnapKit

final class TableViewCellForCollectionView: BTableCell {
    
    let headerLabel: UILabel = {
        let l = UILabel()
        l.textColor = .highlightedYellow
        l.font = UIFont.boldSystemFont(ofSize: 20)
        l.textAlignment = .left
        return l
    }()
    
    let collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let height = 190
        let width = 150
        layout.itemSize = CGSize(width: width, height: height)
        layout.minimumInteritemSpacing = 10
        layout.minimumLineSpacing = 10
        layout.scrollDirection = .horizontal
        
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.contentInset = UIEdgeInsets(top: 0, left: 15, bottom: 0, right: 15)
        cv.backgroundColor = .clear
        cv.showsHorizontalScrollIndicator = false
        return cv
    }()
    
    override func setupView() {
        backgroundColor = .clear
        setupCollectionView()
    }
    
    override func addSubviews() {
        super.addSubviews()
        [headerLabel, collectionView].forEach(addSubview(_:))
    }
    
    override func setupConstraints() {
        super.setupConstraints()
        
        headerLabel.snp.makeConstraints {
            $0.leading.top.equalToSuperview().offset(15)
            $0.trailing.equalToSuperview()
            $0.height.equalTo(20)
        }
        
        collectionView.snp.makeConstraints {
            $0.top.equalTo(headerLabel.snp.bottom).offset(10)
            $0.bottom.equalToSuperview()
            $0.leading.trailing.equalToSuperview()
            $0.height.equalTo(215)
        }
        
    }
    
    private var currenciesForSection = [CurrencyProjection]()
    
    var currencySelected: (CurrencyProjection) -> Void = { _ in }
    
    func setCell(for section: CurrencyAlphabetSection) {
        headerLabel.text = section.header
        currenciesForSection = section.items
        collectionView.reloadData()
    }
    
    private func setupCollectionView() {
        
        collectionView.register(CurrencyCollectionViewCell.self, forCellWithReuseIdentifier: String(describing: CurrencyCollectionViewCell.self))
        
        collectionView.delegate = self
        collectionView.dataSource = self
    }
    
}

extension TableViewCellForCollectionView: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return currenciesForSection.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: CurrencyCollectionViewCell.self), for: indexPath) as! CurrencyCollectionViewCell
        cell.setCurrencyCell(for: currenciesForSection[indexPath.item])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        currencySelected(currenciesForSection[indexPath.item])
    }
    
}
