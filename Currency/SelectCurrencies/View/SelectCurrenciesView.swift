//
//  SelectCurrenciesView.swift
//  Currency
//
//  Created by Robert Dresler on 03/05/2019.
//  Copyright © 2019 Robert Dresler. All rights reserved.
//

import UIKit
import SnapKit

final class SelectCurrenciesView: BView {
    
    let tableView: UITableView = {
        let tv = UITableView()
        tv.backgroundColor = .clear
        tv.contentInset = UIEdgeInsets(top: 15, left: 0, bottom: 15, right: 0)
        tv.rowHeight = 260
        tv.estimatedRowHeight = 260
        tv.separatorStyle = .none
        return tv
    }()
    
    override func setupView() {
        backgroundColor = .darkBrown
    }
    
    override func addSubviews() {
        super.addSubviews()
        [tableView].forEach(addSubview(_:))
    }
    
    override func setupConstraints() {
        super.setupConstraints()
        
        tableView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
        
    }
    
}
