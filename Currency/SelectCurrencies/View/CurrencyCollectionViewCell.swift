//
//  CurrencyCollectionViewCell.swift
//  Currency
//
//  Created by Robert Dresler on 03/05/2019.
//  Copyright © 2019 Robert Dresler. All rights reserved.
//

import UIKit
import SnapKit
import Kingfisher

final class CurrencyCollectionViewCell: BCollectionCell, CurrencyCell {
    
    var cellContentView: UIView {
        return contentView
    }
    
    var flagImageView: UIImageView = {
        let iv = UIImageView()
        iv.clipsToBounds = true
        iv.contentMode = .scaleAspectFill
        iv.backgroundColor = .darkBrown
        iv.layer.cornerRadius = 30
        return iv
    }()
    
    var currencyNameLabel: UILabel = {
        let l = UILabel()
        l.numberOfLines = 2
        l.textColor = .white
        l.font = UIFont.systemFont(ofSize: 17)
        l.textAlignment = .center
        l.adjustsFontSizeToFitWidth = true
        return l
    }()
    
    var highlightedFieldLabel: UILabel = {
        let l = UILabel()
        l.textColor = .white
        l.font = UIFont.boldSystemFont(ofSize: 20)
        l.textAlignment = .center
        return l
    }()
    
    override func setupView() {
        contentView.backgroundColor = .lightBrown
        contentView.layer.cornerRadius = 30
    }
    
    override func addSubviews() {
        [flagImageView, currencyNameLabel, highlightedFieldLabel].forEach(contentView.addSubview(_:))
    }
    
    override func setupConstraints() {
        
        flagImageView.snp.makeConstraints {
            $0.centerX.equalToSuperview()
            $0.top.equalToSuperview().offset(15)
            $0.size.equalTo(60)
        }
        
        currencyNameLabel.snp.makeConstraints {
            $0.top.greaterThanOrEqualTo(flagImageView.snp.bottom).offset(20)
            $0.leading.equalToSuperview().offset(10)
            $0.trailing.equalToSuperview().offset(-10)
        }
        
        highlightedFieldLabel.snp.makeConstraints {
            $0.centerX.equalToSuperview()
            $0.top.equalTo(currencyNameLabel.snp.bottom).offset(20)
            $0.bottom.equalToSuperview().offset(-20)
        }
        
    }
    
}
