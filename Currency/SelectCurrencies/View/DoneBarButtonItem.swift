//
//  DoneBarButtonItem.swift
//  Currency
//
//  Created by Robert Dresler on 03/05/2019.
//  Copyright © 2019 Robert Dresler. All rights reserved.
//

import UIKit

extension UIBarButtonItem {
    
    static var doneItem: UIBarButtonItem {
        let b = UIButton()
        b.setTitle("DONE".localized, for: .normal)
        b.setTitleColor(.highlightedYellow, for: .normal)
        b.titleLabel?.font = UIFont.boldSystemFont(ofSize: 20)
        return UIBarButtonItem(customView: b)
    }
    
}
