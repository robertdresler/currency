//
//  SelectCurrenciesViewController.swift
//  Currency
//
//  Created by Robert Dresler on 03/05/2019.
//  Copyright © 2019 Robert Dresler. All rights reserved.
//

import UIKit
import RxSwift

final class SelectCurrenciesViewController<TView, TViewModel>: BViewController<TView, TViewModel>
    where TView: SelectCurrenciesView,
          TViewModel: SelectCurrenciesViewModel {
    
    override func setupView() {
        super.setupView()
        navigationItem.leftBarButtonItem = .doneItem
        navigationItem.searchController = CurrencySearchController.instantiate()
        navigationItem.hidesSearchBarWhenScrolling = false
    }
    
    override func setupBinding() {
        super.setupBinding()
        
        (navigationItem.leftBarButtonItem?.customView as? UIButton)?.rx.tap.subscribe(onNext: { [weak self] in
            self?.viewModel.doneButtonPressed()
        }).disposed(by: bag)
        
        customView.tableView.register(TableViewCellForCollectionView.self, forCellReuseIdentifier: String(describing: TableViewCellForCollectionView.self))
        
        viewModel.currencySections
            .bind(to: customView.tableView.rx.items(cellIdentifier: String(describing: TableViewCellForCollectionView.self), cellType: TableViewCellForCollectionView.self)) { row, section, cell in
                
                cell.setCell(for: section)
                
                cell.currencySelected = { [weak self] currencyProjection in
                    guard let self = self else { return }
                    self.viewModel.currencySelected(with: currencyProjection.code)
                    UIImpactFeedbackGenerator(style: .medium).impactOccurred()
                }
                
            }.disposed(by: bag)
        
        navigationItem.searchController?.searchBar.rx.text.orEmpty.bind(to: viewModel.searchText).disposed(by: bag)
        
    }
    
}
