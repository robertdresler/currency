//
//  BViewModel.swift
//  Currency
//
//  Created by Robert Dresler on 02/05/2019.
//  Copyright © 2019 Robert Dresler. All rights reserved.
//

import Foundation
import RxSwift

protocol BViewModel: class {
    var bag: DisposeBag { get set }
    init()
}
