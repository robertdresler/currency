//
//  BView.swift
//  Currency
//
//  Created by Robert Dresler on 02/05/2019.
//  Copyright © 2019 Robert Dresler. All rights reserved.
//

import UIKit

class BView: UIView, ViewSetupable {
    
    required init() {
        super.init(frame: .zero)
        callSetups()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupView() {
        backgroundColor = .white
    }
    
    func addSubviews() {}
    func setupSubviews() {}
    func setupConstraints() {}
    
}
