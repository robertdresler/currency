//
//  BCell.swift
//  Currency
//
//  Created by Robert Dresler on 03/05/2019.
//  Copyright © 2019 Robert Dresler. All rights reserved.
//

import UIKit

class BTableCell: UITableViewCell, ViewSetupable {
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        callSetups()
        selectionStyle = .none
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupView() {}
    
    func addSubviews() {}
    
    func setupSubviews() {}
    
    func setupConstraints() {}
    
}

class BCollectionCell: UICollectionViewCell, ViewSetupable {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        callSetups()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupView() {}
    
    func addSubviews() {}
    
    func setupSubviews() {}
    
    func setupConstraints() {}
    
}
