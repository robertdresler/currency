//
//  ViewSetupable.swift
//  Currency
//
//  Created by Robert Dresler on 02/05/2019.
//  Copyright © 2019 Robert Dresler. All rights reserved.
//

import UIKit

protocol ViewSetupable where Self: UIView {
    func setupView()
    func addSubviews()
    func setupSubviews()
    func setupConstraints()
}

extension ViewSetupable {
    
    func callSetups() {
        setupView()
        addSubviews()
        setupSubviews()
        setupConstraints()
    }
    
}

