//
//  BViewController.swift
//  Currency
//
//  Created by Robert Dresler on 02/05/2019.
//  Copyright © 2019 Robert Dresler. All rights reserved.
//

import UIKit
import RxSwift

class BViewController<TView: BView, TViewModel: BViewModel>: UIViewController {
    
    /**
     Property for generic TView which is subclass of BView
     */
    var customView: TView {
        guard let view = view as? TView else { fatalError("view isn't \(String(describing: TView.self))") }
        return view
    }
    
    /**
     Property for generic TViewModel which BViewController has to implement
     */
    var viewModel: TViewModel {
        guard let vm = vm else { fatalError("TViewModel hasn't been implemented")}
        return vm
    }
    
    private var vm: TViewModel?
    
    /**
     DisposeBag for RxSwift binding
    */
    var bag = DisposeBag()
    
    init(setupViewModel: (TViewModel) -> Void = { _ in }) {
        super.init(nibName: nil, bundle: nil)
        vm = TViewModel()
        setupViewModel(viewModel)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func loadView() {
        view = TView()
        setupView()
        setupBinding()
    }
    
    func setupView() {}
    
    func setupBinding() {}
    
}
