//
//  AppDelegate.swift
//  Currency
//
//  Created by Robert Dresler on 02/05/2019.
//  Copyright © 2019 Robert Dresler. All rights reserved.
//

import UIKit
import RealmSwift

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    private var currenciesInitalizedKey = "currenciesInitialized"

    var window: UIWindow?
    
    private var coordinator: CurrencyCoorindator!

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        print("Realm database file location: \(Realm.Configuration.defaultConfiguration.fileURL!)")
        
        do {
            _ = try Realm()
        } catch { print("Error initialising new realm, \(error)") }
        
        checkIfShouldInitializeCurrencies()
        
        window = UIWindow(frame: UIScreen.main.bounds)
        
        window?.tintColor = .white
        
        startApp()
        
        return true
    }
    
    private func startApp() {
        coordinator = CurrencyCoorindator()
        window?.rootViewController = coordinator.navigationController
        window?.makeKeyAndVisible()
        coordinator.start(animated: false)
    }
    
    private func checkIfShouldInitializeCurrencies() {
        if UserDefaults.standard.bool(forKey: currenciesInitalizedKey) == false {
            CurrencyManagerFactory.manager.saveCurrenciesFromFile { [weak self] success in
                guard let self = self else { return }
                print(success ? "Successfully initalized currencies" : "Currencies couldn't be initialized")
                if success {
                    UserDefaults.standard.set(true, forKey: self.currenciesInitalizedKey)
                }
            }
        }
    }

}

