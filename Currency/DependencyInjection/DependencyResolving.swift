//
//  DependencyResolving.swift
//  Currency
//
//  Created by Robert Dresler on 03/05/2019.
//  Copyright © 2019 Robert Dresler. All rights reserved.
//

protocol DependencyResolving {
    func resolveConvertCurrencyController() -> ConvertCurrencyViewController<ConvertCurrencyView, ConvertCurrencyViewModel>
    func resolveSelectCurrenciesController() -> SelectCurrenciesViewController<SelectCurrenciesView, SelectCurrenciesViewModel>
}

extension DependencyResolving {
    
    func resolveConvertCurrencyController() -> ConvertCurrencyViewController<ConvertCurrencyView, ConvertCurrencyViewModel> {
        return ConvertCurrencyViewController()
    }
    
    func resolveSelectCurrenciesController() -> SelectCurrenciesViewController<SelectCurrenciesView, SelectCurrenciesViewModel> {
        return SelectCurrenciesViewController()
    }
    
}
