//
//  CurrencyCoordinator.swift
//  Currency
//
//  Created by Robert Dresler on 03/05/2019.
//  Copyright © 2019 Robert Dresler. All rights reserved.
//

import UIKit

class CurrencyCoorindator {
    
    var dependencyContainer = DependencyContainer()
    
    lazy var navigationController: CurrencyNavigationController = {
        let nc = CurrencyNavigationController()
        nc.setForConvertCurrencyView()
        return nc
    }()
    
    lazy var baseController: ConvertCurrencyViewController = {
        let vc = dependencyContainer.resolveConvertCurrencyController()
        vc.addButtonPressed = { [weak self] in
            self?.showSelectCurrenciesVC()
        }
        vc.title = "APP_NAME".localized
        return vc
    }()
    
    func start(animated: Bool) {
        navigationController.pushViewController(baseController, animated: animated)
    }
    
    private func showSelectCurrenciesVC() {
        let selectCurrenciesVC = dependencyContainer.resolveSelectCurrenciesController()
        selectCurrenciesVC.viewModel.doneButtonPressed = { [weak self] in
            self?.hideSelectCurrenciesVC()
        }
        selectCurrenciesVC.title = "ADD".localized
        navigationController.setForSelectCurrenciesView()
        navigationController.pushViewController(selectCurrenciesVC, animated: true)
    }
    
    private func hideSelectCurrenciesVC() {
        navigationController.popViewController(animated: true)
        navigationController.setForConvertCurrencyView()
    }
    
}
