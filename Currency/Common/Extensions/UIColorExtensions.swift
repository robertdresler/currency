//
//  UIColorExtensions.swift
//  Currency
//
//  Created by Robert Dresler on 02/05/2019.
//  Copyright © 2019 Robert Dresler. All rights reserved.
//

import UIKit

extension UIColor {
    
    static let darkBrown = UIColor(red: 47/255, green: 41/255, blue: 52/255, alpha: 1)
    static let lightBrown = UIColor(red: 56/255, green: 51/255, blue: 61/255, alpha: 1)
    static let highlightedYellow = UIColor(red: 255/255, green: 208/255, blue: 61/255, alpha: 1)
    
}
