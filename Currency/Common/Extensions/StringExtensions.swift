//
//  StringExtensions.swift
//  Currency
//
//  Created by Robert Dresler on 02/05/2019.
//  Copyright © 2019 Robert Dresler. All rights reserved.
//

import Foundation

extension String {
    
    var localized: String {
        return NSLocalizedString(self, comment: "")
    }
    
}
