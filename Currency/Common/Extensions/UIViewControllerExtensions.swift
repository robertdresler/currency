//
//  UIViewControllerExtensions.swift
//  Currency
//
//  Created by Robert Dresler on 03/05/2019.
//  Copyright © 2019 Robert Dresler. All rights reserved.
//

import UIKit

extension UIViewController {
    
    func showAlert(title: String? = nil, message: String? = nil, dismissAfter: Double = 1.3) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        present(alert, animated: true) {
            DispatchQueue.main.asyncAfter(deadline: .now() + dismissAfter) {
                alert.dismiss(animated: true)
            }
        }
    }
    
}
