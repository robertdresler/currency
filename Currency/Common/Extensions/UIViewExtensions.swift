//
//  UIViewExtensions.swift
//  Currency
//
//  Created by Robert Dresler on 03/05/2019.
//  Copyright © 2019 Robert Dresler. All rights reserved.
//

import UIKit

extension UIView {
    
    func showWithAnimation(duration: Double = 0.3) {
        UIView.animate(withDuration: duration) {
            self.alpha = 1
        }
    }
    
    func hideWithAnimation(duration: Double = 0.3) {
        UIView.animate(withDuration: duration) {
            self.alpha = 0
        }
    }
    
}
