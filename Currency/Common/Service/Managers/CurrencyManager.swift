//
//  CurrencyManager.swift
//  Currency
//
//  Created by Robert Dresler on 02/05/2019.
//  Copyright © 2019 Robert Dresler. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift
import RealmSwift

enum CurrencyManagerFactory {
    static var manager = CurrencyManager()
}

class CurrencyManager {
    
    let currencies = BehaviorRelay<[Currency]>(value: [])
    
    private let realm = try! Realm()
    
    private var results: Results<Currency>?
    
    private var token: NotificationToken?
    
    fileprivate init() {
        results = realm.objects(Currency.self)
        token = results?.observe { [weak self] _ in
            guard let self = self, let results = self.results else { return }
            self.currencies.accept(Array(results))
        }
        guard let results = results else { return }
        currencies.accept(Array(results))
    }
    
    func toogleIsSelected(of currency: Currency) {
        do {
            try realm.write {
                if currency.isSelected { currency.isBase = false }
                currency.isSelected = !currency.isSelected
            }
        } catch { print(error) }
    }
    
    func setIsBase(of currency: Currency) {
        do {
            try realm.write {
                currencies.value.forEach { $0.isBase = false }
                currency.isBase = true
            }
        } catch { print(error) }
    }
    
    func saveCurrenciesFromFile(completion: @escaping (Bool) -> Void) {
        
        guard let url = Bundle.main.url(forResource: "currencies", withExtension: "json") else { return completion(false) }
        
        do {
            let data = try Data(contentsOf: url)
            let decoder = JSONDecoder()
            decoder.keyDecodingStrategy = .convertFromSnakeCase
            let decoded = try decoder.decode([Currency].self, from: data)
            try realm.write {
                decoded.sorted(by: { $0.name < $1.name }).forEach { realm.add($0) }
            }
            completion(true)
        } catch {
            print(error)
            completion(false)
        }
    }
    
}
