//
//  ExchangeProvider.swift
//  Currency
//
//  Created by Robert Dresler on 03/05/2019.
//  Copyright © 2019 Robert Dresler. All rights reserved.
//

import Foundation
import Alamofire
import RxCocoa

class ExchangeManager {
    
    let state = BehaviorRelay<ExchangeState>(value: .initial)
    
    init() {}
    
    func loadData() {
        if case .loading = state.value { return }
        
        getExchange { [weak self] result in
            switch result {
            case .success(let exchange):
                self?.state.accept(.dataReceived(exchange))
            case .failure(let error):
                self?.state.accept(.error(error))
                print(error)
            }
        }
    }
    
    private func getExchange(completion: @escaping (Swift.Result<Exchange, ExchangeError>) -> Void) {
        
        guard let base = CurrencyManagerFactory.manager.currencies.value.first(where: { $0.isBase }) else {
            return completion(.failure(.noBaseCurrency))
        }
        
        let codes = CurrencyManagerFactory.manager.currencies.value.compactMap { (currency: Currency) -> String? in
            return currency.isSelected && !currency.isBase ? currency.code : nil
        }
        
        guard !codes.isEmpty else {
            return completion(.failure(.notEnoughSelectedCurrencies))
        }
            
        let request = RequestProvider().exchangeRequest(baseCode: base.code, exchangeCodes: codes)
        Alamofire.request(request).responseData(queue: .global()) { response in
            
            switch response.result {
            case .success(let data):
                
                do {
                    let exchange = try JSONDecoder().decode(Exchange.self, from: data)
                    DispatchQueue.main.async { completion(.success(exchange)) }
                } catch {
                    DispatchQueue.main.async { completion(.failure(.wrongModel)) }
                }
                
            case .failure:
                DispatchQueue.main.async { completion(.failure(.apiError)) }
            }
            
        }
        
    }
    
}
