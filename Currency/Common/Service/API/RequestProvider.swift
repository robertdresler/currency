//
//  RequestProvider.swift
//  Currency
//
//  Created by Robert Dresler on 03/05/2019.
//  Copyright © 2019 Robert Dresler. All rights reserved.
//

import Foundation

struct RequestProvider {
    
    let baseUrlWithAccessKeyString = "https://api.exchangeratesapi.io/latest"
    
    func exchangeRequest(baseCode: String, exchangeCodes: [String]) -> URLRequest {
        var finalUrlString = baseUrlWithAccessKeyString
        
        finalUrlString += "?base=\(baseCode)"
        
        guard let url = URL(string: finalUrlString) else { fatalError("URL cannot be created from \(finalUrlString)") }
        return URLRequest(url: url)
    }
    
}
