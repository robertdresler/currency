//
//  ExchangeState.swift
//  Currency
//
//  Created by Robert Dresler on 03/05/2019.
//  Copyright © 2019 Robert Dresler. All rights reserved.
//

enum ExchangeState {
    case initial
    case loading
    case dataReceived(Exchange)
    case error(ExchangeError)
}
