//
//  ExchangeError.swift
//  Currency
//
//  Created by Robert Dresler on 03/05/2019.
//  Copyright © 2019 Robert Dresler. All rights reserved.
//

enum ExchangeError: Error {
    case apiError
    case wrongModel
    case noBaseCurrency
    case notEnoughSelectedCurrencies
    
    var message: String {
        switch self {
        case .apiError:
            return "API_ERROR".localized
        case .wrongModel:
            return "WRONG_MODEL".localized
        case .noBaseCurrency:
            return "NO_BASE_CURRENCY".localized
        case .notEnoughSelectedCurrencies:
            return "NOT_ENOUGH_SELECTED_CURRENCIES".localized
        }
    }
}
