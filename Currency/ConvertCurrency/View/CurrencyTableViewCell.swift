//
//  CurrencyTableViewCell.swift
//  Currency
//
//  Created by Robert Dresler on 03/05/2019.
//  Copyright © 2019 Robert Dresler. All rights reserved.
//

import UIKit
import SnapKit
import RxCocoa
import RxSwift

final class CurrencyTableViewCell: BTableCell, CurrencyCell {
    
    var cellContentView: UIView {
        return customContentView
    }
    
    private let customContentView: UIView = {
        let v = UIView()
        v.layer.cornerRadius = 30
        v.backgroundColor = .lightBrown
        return v
    }()
    
    var flagImageView: UIImageView = {
        let iv = UIImageView()
        iv.clipsToBounds = true
        iv.contentMode = .scaleAspectFill
        iv.backgroundColor = .darkBrown
        iv.layer.cornerRadius = 20
        return iv
    }()
    
    var currencyNameLabel: UILabel = {
        let l = UILabel()
        l.textColor = .white
        l.font = UIFont.systemFont(ofSize: 17)
        l.textAlignment = .left
        l.adjustsFontSizeToFitWidth = true
        return l
    }()
    
    var highlightedFieldLabel: UILabel = {
        let l = UILabel()
        l.textColor = .white
        l.font = UIFont.boldSystemFont(ofSize: 20)
        l.textAlignment = .right
        l.setContentCompressionResistancePriority(.required, for: .horizontal)
        return l
    }()
    
    override func setupView() {
        backgroundColor = .clear
    }
    
    override func addSubviews() {
        contentView.addSubview(customContentView)
        [flagImageView, currencyNameLabel, highlightedFieldLabel].forEach(customContentView.addSubview(_:))
    }

    override func setupConstraints() {
        
        customContentView.snp.makeConstraints {
            $0.top.equalToSuperview().offset(5)
            $0.bottom.equalToSuperview().offset(-5)
            $0.leading.equalToSuperview().offset(15)
            $0.trailing.equalToSuperview().offset(-15)
        }
        
        flagImageView.snp.makeConstraints {
            $0.centerY.equalToSuperview()
            $0.top.leading.equalToSuperview().offset(20)
            $0.bottom.equalToSuperview().offset(-20)
            $0.size.equalTo(40)
        }
        
        currencyNameLabel.snp.makeConstraints {
            $0.centerY.equalToSuperview()
            $0.leading.equalTo(flagImageView.snp.trailing).offset(15)
        }
        
        highlightedFieldLabel.snp.makeConstraints {
            $0.centerY.equalToSuperview()
            $0.leading.equalTo(currencyNameLabel.snp.trailing).offset(15)
            $0.trailing.equalToSuperview().offset(-20)
        }
        
    }
    
}
