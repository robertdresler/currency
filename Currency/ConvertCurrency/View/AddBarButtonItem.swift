//
//  AddBarButtonItem.swift
//  Currency
//
//  Created by Robert Dresler on 03/05/2019.
//  Copyright © 2019 Robert Dresler. All rights reserved.
//

import UIKit

extension UIBarButtonItem {
    
    static var addItem: UIBarButtonItem {
        let b = UIButton()
        b.setImage(UIImage(named: "icAdd")!, for: .normal)
        b.imageView?.tintColor = .highlightedYellow
        return UIBarButtonItem(customView: b)
    }
    
}
