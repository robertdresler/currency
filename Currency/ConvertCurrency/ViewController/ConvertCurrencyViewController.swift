//
//  ConvertCurrencyViewController.swift
//  Currency
//
//  Created by Robert Dresler on 02/05/2019.
//  Copyright © 2019 Robert Dresler. All rights reserved.
//

import UIKit

final class ConvertCurrencyViewController<TView, TViewModel>: BViewController<TView, TViewModel>
    where TView: ConvertCurrencyView,
          TViewModel: ConvertCurrencyViewModel {
    
    var addButtonPressed: () -> Void = {}
    
    override func setupView() {
        super.setupView()
        navigationItem.leftBarButtonItem = .addItem
    }
    
    override func setupBinding() {
        super.setupBinding()
        
        customView.tableView.register(CurrencyTableViewCell.self, forCellReuseIdentifier: String(describing: CurrencyTableViewCell.self))
        
        viewModel.currenciesProjections.bind(to: customView.tableView.rx.items(cellIdentifier: String(describing: CurrencyTableViewCell.self), cellType: CurrencyTableViewCell.self)) { row, projection, cell in
            cell.setCurrencyCell(for: projection)
        }.disposed(by: bag)
        
        customView.tableView.rx.modelSelected(CurrencyProjection.self).bind { [weak self] projection in
            guard let self = self else { return }
            self.viewModel.currencySelected(with: projection.code)
            UIImpactFeedbackGenerator(style: .medium).impactOccurred()
        }.disposed(by: bag)
        
        viewModel.errorMessage.bind(onNext: { [weak self] message in
            guard let message = message else { return }
            self?.showErrorAlert(message: message)
        }).disposed(by: bag)
        
        (navigationItem.leftBarButtonItem?.customView as? UIButton)?.rx.tap.subscribe(onNext: { [weak self] in
            self?.addButtonPressed()
        }).disposed(by: bag)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if let message = viewModel.errorMessage.value {
            showErrorAlert(message: message)
        }
    }
    
    private func showErrorAlert(message: String) {
        if navigationController?.topViewController == self {
            showAlert(title: message)
        }
    }
    
}
