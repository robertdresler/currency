//
//  ConvertCurrencyViewModel.swift
//  Currency
//
//  Created by Robert Dresler on 02/05/2019.
//  Copyright © 2019 Robert Dresler. All rights reserved.
//

import RxSwift
import RxCocoa

final class ConvertCurrencyViewModel: CurrencyViewModel {
    
    var exchangeProvider = ExchangeManager()
    
    var manager = CurrencyManagerFactory.manager
    var currencySelected: (Currency) -> Void = { _ in }
    
    var bag = DisposeBag()
    
    let currenciesProjections = BehaviorRelay<[CurrencyProjection]>(value: [])
    
    let state = BehaviorRelay<ExchangeState>(value: .initial)
    
    let errorMessage = BehaviorRelay<String?>(value: nil)
    
    required init() {
        setupBinding()
    }
    
    private func setupBinding() {
        manager.currencies.bind { [weak self] currencies in

            let projections = currencies.filter({ $0.isSelected }).map({
                CurrencyProjection(code: $0.code, name: $0.name, highlightedField: "---", flagImageUrl: $0.flagImageURL, isHighlighted: $0.isBase)
            })
            
            self?.loadData()
            
            self?.currenciesProjections.accept(projections)
            
        }.disposed(by: bag)
        
        exchangeProvider.state.bind(to: state).disposed(by: bag)
        
        currencySelected = { [weak self] currency in
            self?.manager.setIsBase(of: currency)
        }
        
        state.bind(onNext: { [weak self] state in
            
            self?.errorMessage.accept(nil)

            switch state {
            case .initial:
                break
            case .loading:
                break
            case .error(let error):
                self?.errorMessage.accept(error.message)
            case .dataReceived(let exchange):
                self?.processExchange(exchange)
            }
        }).disposed(by: bag)
    }
    
    private func loadData() {
        exchangeProvider.loadData()
    }
    
    private func processExchange(_ exchange: Exchange) {
        
        let localizedExchange = self.localizedExchange(from: exchange)
        
        let projections = manager.currencies.value.filter({ $0.isSelected }).map({
            CurrencyProjection(code: $0.code, name: $0.name, highlightedField: localizedExchange[$0.code] ?? "---", flagImageUrl: $0.flagImageURL, isHighlighted: $0.isBase)
        })
        
        currenciesProjections.accept(projections)
    }
    
    private func localizedExchange(from exchange: Exchange) -> [String: String] {
        return Dictionary(uniqueKeysWithValues: manager.currencies.value.filter({ $0.isSelected }).compactMap { currency in
            
            if currency.isBase {
                guard let rate = formattedRate(1, digits: currency.decimalDigits, code: currency.code) else { return nil }
                return (currency.code, rate)
            } else {
                guard let rate = exchange.rates[currency.code],
                    let formattedRate = formattedRate(rate, digits: currency.decimalDigits, code: currency.code)
                else { return nil }
                return (currency.code, formattedRate)
            }
        })
    }
    
    private func formattedRate(_ rate: Double, digits: Double, code: String) -> String? {
        formatter.maximumFractionDigits = Int(digits)
        formatter.minimumFractionDigits = Int(digits)
        formatter.currencyCode = code
        guard let formatted = formatter.string(from: NSNumber(value: rate)) else { return nil }
        return formatted
    }
    
    private let formatter: NumberFormatter = {
        let f = NumberFormatter()
        f.numberStyle = .currency
        return f
    }()
    
}
