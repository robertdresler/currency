//
//  CurrencyNavigationController.swift
//  Currency
//
//  Created by Robert Dresler on 02/05/2019.
//  Copyright © 2019 Robert Dresler. All rights reserved.
//

import UIKit
import SnapKit

final class CurrencyNavigationController: UINavigationController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupNavigationBar()
        setupNavigationItem()
    }
    
    func setForConvertCurrencyView() {
        navigationBar.prefersLargeTitles = true
    }
    
    func setForSelectCurrenciesView() {
        navigationBar.prefersLargeTitles = false
    }
    
    private func setupNavigationBar() {
        navigationBar.barStyle = .black
        navigationBar.barTintColor = .darkBrown
        navigationBar.isTranslucent = false
    }
    
    private func setupNavigationItem() {
        navigationItem.hidesBackButton = true
    }
    
}
