//
//  CurrencySearchController.swift
//  
//
//  Created by Robert Dresler on 03/05/2019.
//

import UIKit

final class CurrencySearchController: UISearchController {
    
    static func instantiate() -> CurrencySearchController {
        let sc = CurrencySearchController(searchResultsController: nil)
        sc.searchBar.placeholder = "SEARCH_ALL".localized
        sc.dimsBackgroundDuringPresentation = false
        return sc
    }
    
}
