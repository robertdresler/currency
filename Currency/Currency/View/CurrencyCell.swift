//
//  CurrencyCell.swift
//  Currency
//
//  Created by Robert Dresler on 03/05/2019.
//  Copyright © 2019 Robert Dresler. All rights reserved.
//

import UIKit
import Kingfisher

protocol CurrencyCell {
    var flagImageView: UIImageView { get set }
    var currencyNameLabel: UILabel { get set }
    var highlightedFieldLabel: UILabel { get set }
    var cellContentView: UIView { get }
}

extension CurrencyCell {
    
    func setCurrencyCell(for currencyProjection: CurrencyProjection) {
        flagImageView.kf.setImage(with: currencyProjection.flagImageUrl)
        currencyNameLabel.text = currencyProjection.name
        highlightedFieldLabel.text = currencyProjection.highlightedField
        
        currencyNameLabel.textColor = currencyProjection.isHighlighted ? .black : .white
        highlightedFieldLabel.textColor = currencyProjection.isHighlighted ? .black : .white
        cellContentView.backgroundColor = currencyProjection.isHighlighted ? .highlightedYellow : .lightBrown
    }
    
}
