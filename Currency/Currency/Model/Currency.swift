//
//  Currency.swift
//  Currency
//
//  Created by Robert Dresler on 02/05/2019.
//  Copyright © 2019 Robert Dresler. All rights reserved.
//

import Foundation
import RealmSwift

class Currency: Object, Decodable {
    
    @objc dynamic var symbol: String = ""
    @objc dynamic var name: String = ""
    @objc dynamic var symbolNative: String = ""
    @objc dynamic var code: String = ""
    @objc dynamic var namePlural: String = ""
    @objc dynamic var flag: String = ""
    @objc dynamic var decimalDigits: Double = 0
    @objc dynamic var rounding: Double = 0
    @objc dynamic var isSelected: Bool = false
    @objc dynamic var isBase: Bool = false
    
    var flagImageURL: URL {
        guard let url = URL(string: flag) else { fatalError("Unable to create URL from \(flag)")}
        return url
    }
    
    private enum CodingKeys: String, CodingKey { case symbol, name, symbolNative, code, namePlural, flag, decimalDigits, rounding }
    
    required convenience init(from decoder: Decoder) throws {
        self.init()
        let container = try decoder.container(keyedBy: CodingKeys.self)
        symbol = try container.decode(String.self, forKey: .symbol)
        name = try container.decode(String.self, forKey: .name)
        symbolNative = try container.decode(String.self, forKey: .symbolNative)
        code = try container.decode(String.self, forKey: .code)
        namePlural = try container.decode(String.self, forKey: .namePlural)
        flag = try container.decode(String.self, forKey: .flag)
        decimalDigits = try container.decode(Double.self, forKey: .decimalDigits)
        rounding = try container.decode(Double.self, forKey: .rounding)
    }
    
}
