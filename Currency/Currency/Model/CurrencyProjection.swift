//
//  CurrencyProjection.swift
//  Currency
//
//  Created by Robert Dresler on 03/05/2019.
//  Copyright © 2019 Robert Dresler. All rights reserved.
//

import Foundation

struct CurrencyProjection {
    let code, name, highlightedField: String
    let flagImageUrl: URL
    let isHighlighted: Bool
}
