//
//  CurrencyViewModel.swift
//  Currency
//
//  Created by Robert Dresler on 03/05/2019.
//  Copyright © 2019 Robert Dresler. All rights reserved.
//

import Foundation

protocol CurrencyViewModel: BViewModel {
    var manager: CurrencyManager { get set }
    var currencySelected: (Currency) -> Void { get set }
}

extension CurrencyViewModel {
    
    /**
     Returns database object for Currency from code of selected CurrencyProjection
     */
    func currencySelected(with code: String) {
        if let currency = manager.currencies.value.first(where: { $0.code == code }) {
            currencySelected(currency)
        }
    }
    
}
